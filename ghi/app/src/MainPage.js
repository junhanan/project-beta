import React from "react"
import car_image2 from "./images/car_image2.jpg";
import "./index.css"

function MainPage() {
  return (
    <div className="main">
      <div className="text-container">
        {/* <img src={car_image2} alt="Front Page" /> */}
        <h1 className="display-5 fw-bold text-white text-center">RevveD</h1>
        <div className="col-lg-12 mx-auto">
          <p className="lead mb-4 text-white text-bold">
            The premiere solution for automobile dealership
            management!
          </p>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
