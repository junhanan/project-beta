// import React from 'react';
// import PropTypes from 'prop-types';
// //import { Test } from './ProfileCard.styles';

// const ProfileCard = (props) => (
//   <div classNameName="ProfileCardWrapper">
//     {/* Display profile information */}
//     <h2>{props.name}</h2>
//     <p>{props.age}</p>
//     <img src={props.image} alt="Profile" />
//     <p>State: {props.state}</p>
//     {/* Add more profile details */}
//   </div>
// );

// ProfileCard.propTypes = {
//   name: PropTypes.string.isRequired,
//   age: PropTypes.number.isRequired,
//   image: PropTypes.string.isRequired,
//   state: PropTypes.string.isRequired,
//   // Add more prop types as needed
// };

// ProfileCard.defaultProps = {
//   // Set default values for props if necessary
// };

// export default ProfileCard;

import React from 'react';
import PropTypes from 'prop-types';
//import { Test } from './ProfileCard.styles';

const ProfileCard = (props) => (
  <div className="card">
    <img src="..." className="card-img-top" />
    <div className="card-body">
      <h5 className="card-title">{props.name}</h5>
      <p className="card-text">{props.bio}</p>
    </div>
    <div className="card-footer">
      <small className="text-muted">{props.age}    {props.state}</small>
    </div>
  </div>
);

// Add prop types
ProfileCard.propTypes = {
  name: PropTypes.string.isRequired,
  age: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  // Add more prop types as needed
};

// Add default props
ProfileCard.defaultProps = {
  // Set default values for props if necessary
};

// Export component
export default ProfileCard;
