import React, { useState, useEffect } from 'react'


const initialFormState = {
    vin: '',
    customer: '',
    date: '',
    time: '',
    technician: '',
    reason: '',
}

export default function AppointmentForm() {
    const [formData, setFormData] = useState(initialFormState)
    const [technicians, setTechnicians] = useState([])

    const fetchData = async () => {
        let url = 'http://localhost:8080/api/technicians/'
        let response = await fetch(url)

        if (response.ok){
            let data = await response.json()
            setTechnicians(data.technicians)
        }
    }
    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (e) => {
        e.preventDefault()

        const datetime = new Date(`${formData.date} ${formData.time}`).toISOString()

        const updateFormData = {
            ...formData,
            date_time: datetime
        }

        const {date, time, ...postData} = updateFormData

        let url = 'http://localhost:8080/api/appointments/'

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(postData),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        let response = await fetch(url, fetchConfig)

        if (response.ok){
            setFormData(initialFormState)
        }
    }

    const handleFormChange = (e) => {
        setFormData({
            ...formData, [e.target.name]: e.target.value
        })
    }

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Service Appointment</h1>
                        <form onSubmit={handleSubmit} id="appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} placeholder="Vin" required minLength="17" maxLength="17" type="vin" name="vin" id="vin" className="form-control" value={formData.vin} />
                                <label htmlFor="vin">Enter VIN...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} placeholder="Customer Name" required type="text" name="customer" id="customer" className="form-control" value={formData.customer}/>
                                <label htmlFor="customer">Customer name...</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} placeholder="mm/dd/yyyy" required type="date" name="date" id="date" className="form-control" value={formData.date}/>
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} placeholder="--:--" required type="time" name="time" id="time" className="form-control" value={formData.time}/>
                                <label htmlFor="time">Time</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleFormChange} placeholder="Choose a technician" required name="technician" id="technician" className="form-control" value={formData.technician}>
                                    <option value=''>Choose a technician...</option>
                                    {technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" value={formData.reason}/>
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}








// @router.get("/api/protected", response_model=bool)
// async def get_protected(
//     account_data: dict = Depends(authenticator.get_current_account_data),
// ):
//     return True


// @router.get("/token", response_model=AccountToken | None)
// async def get_token(
//     request: Request,
//     account: UserDB = Depends(authenticator.try_get_current_account_data),
// ) -> AccountToken | None:
//     if account and authenticator.cookie_name in request.cookies:
//         return {
//             "access_token": request.cookies[authenticator.cookie_name],
//             "type": "Bearer",
//             "account": account,
//         }


// @router.post("/api/users", response_model=AccountToken | HttpError)
// async def create_user(
//     info: UserIn,
//     request: Request,
//     response: Response,
//     users: UserQueries = Depends(),
// ):
//     hashed_password = authenticator.hash_password(info.password)
//     try:
//         account = users.create_user(info, hashed_password)
//     except DuplicateUserError:
//         raise HTTPException(
//             status_code=status.HTTP_400_BAD_REQUEST,
//             detail="Cannot create an account with those credentials",
//         )
//     form = AccountForm(username=info.username, password=info.password)
//     token = await authenticator.login(response, request, form, users)
//     return AccountToken(account=account, **token.dict())


// @router.delete("/api/users/{user_id}", response_model=bool)
// def delete_user(
//     user_id: int,
//     response: Response,
//     queries: UserQueries = Depends(),
// ):
//     user = queries.get_user_by_id(user_id)
//     if user is None:
//         raise HTTPException(
//             status_code=status.HTTP_404_NOT_FOUND,
//             detail="User not found",
//         )

//     queries.delete_user(user_id)
//     return True


// @router.get("/api/users", response_model=UsersOut)
// def get_users(queries: UserQueries = Depends()):
//     users = queries.get_users()
//     return {"users": users}


// @router.get("/api/users/{user_id}", response_model=UserOut)
// def get_user_by_id(
//     user_id: int,
//     queries: UserQueries = Depends(),
// ):
//     user = queries.get_user_by_id(user_id)
//     if user is None:
//         raise HTTPException(
//             status_code=status.HTTP_404_NOT_FOUND,
//             detail="User not found",
//         )
//     return user




// @router.put("/api/users/{user_id}", response_model=UserOut)
// def update_user(
//     user_id: int,
//     user_in: UserIn,
//     response: Response,
//     queries: UserQueries = Depends(),
// ):
//     user = queries.get_user_by_id(user_id)
//     if user is None:
//         raise HTTPException(
//             status_code=status.HTTP_404_NOT_FOUND,
//             detail="User not found",
//         )
//     updated_user = queries.update_user(user_id, user_in)
//     return updated_user







// # def test_create_user():
// #     user_info = {
// #         "username": "testuser",
// #         "password": "testpassword",
// #         "email": "test@example.com",
// #         "full_name": "Test User",
// #         "age": 25,
// #         "mbti:": "INTJ",
// #         "bio": "I am a test user.",
// #     }
// #     response = client.post("/api/users", json=user_info)
// #     assert response.status_code == 200
// #     assert "access_token" in response.json()
// #     assert response.json()["account"]["username"] == "testuser"


// # def test_delete_user():
// #     response = client.delete("/api/users/1")
// #     assert response.status_code == 200
// #     assert response.json() is True


// # def test_update_user():
// #     user_info = {"username": "updateduser", "password": "updatedpassword"}
// #     response = client.put("/api/users/1", json=user_info)
// #     assert response.status_code == 200
// #     assert "username" in response.json()
// #     assert response.json()["username"] == "updateduser"
